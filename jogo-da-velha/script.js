var vezJogador = 1;        // vezJogador == 1 irá ser o "X" e vezJogador == 2 será "O"
var acabouPartida = 0;    // acabouPartida sinaliza se o jogo acabou ( == 1) ou se o jogo ainda está acontecendo ( == 0) 
var contadorVelha = 0;   // contador para saber quando ele tiver colocado em todos os espaços vazios
var stringVezDe = document.getElementById("indicadorDaVez");

// Realiza uma jogada
function selecionar(botao) {
    if(botao.innerHTML == "" && acabouPartida == 0){
        contadorVelha ++;
        
        //se vezJogador == 1, siginifica que vai ser colocado um X e a proxima vez é a do O, e é feito o print na tela informando isso
        if(vezJogador == 1 ){
            stringVezDe.innerHTML = "É a vez do O";
            botao.innerHTML = "X";
            vezJogador = 2;
        }
        //se vezJogador == 2, siginifica que vai ser colocado um O e a proxima vez é a do X, e é feito o print na tela informando isso
        else if(vezJogador == 2){
            stringVezDe.innerHTML = "É a vez do X";
            botao.innerHTML = "O";
            vezJogador = 1;
        }
        /*for para verificar a posição da jogada que acabou de ser feita
        Com isso, ele chama a função ganhou e indica a posição da ultima jogada, para verificar se ganhou o jogo*/
        for(var i = 0; i < 9 ; i++){
            if(botao == document.getElementsByClassName("casa")[i]){
                ganhou(i);
            }
        }
    }
    /*Verifica se já houve as 9 jogadas e se o jogou ainda não acabou
    Caso, já tenha acontecido as 9 jogadas e o jogo ainda não tiver acabado, a afirmação do if será verdadeira
    então acabouPartida <- 1, será printado na tela que acabou a partida e que deu velha */
    if(contadorVelha == 9 && acabouPartida == 0){
        acabouPartida = 1;
        stringVezDe.innerHTML = "Acabou a partida";
        document.getElementById("indicadorVencedor").innerHTML = "Deu Velha ";
    }
    
}

// Zera todos as posições e recomeça o jogo
function resetar(){
    for(var i = 0; i < 9; i++){
        document.getElementsByClassName("casa")[i].innerHTML = "";
    }
    stringVezDe.innerHTML = "É a vez do X";
    document.getElementById("indicadorVencedor").innerHTML = "";
    acabouPartida = 0;
    contadorVelha = 0;
    vezJogador = 1;
}

// Verifica todos as combinações possíveis de se ganhar o jogo em torno da ultima jogada feita
function ganhou(posLastJogada) {

    var jogada = document.getElementsByClassName("casa")[posLastJogada].innerHTML; // se a jogada foi X ou O

    /* Vetores com todas as linhas,colunas e diagonais que dá pra vencer 
    A cada elemento do vetor é um possivel indice do vetor de botões*/
    var diagPrin = [0,4,8];
    var diagSec  = [2,4,6];
    var linha1   = [0,1,2];
    var linha2   = [3,4,5];
    var linha3   = [6,7,8];
    var coluna1  = [0,3,6];
    var coluna2  = [1,4,7];
    var coluna3  = [2,5,8];

    /* for para verificar se a ultima jogada ta dentro de quais vetores
    Depois, quando encontrar, o indice referente a ultima jogada é retirado do vetor, e com isso
    verifica-se os outros dois também estão marcados igual a ultima jogada*/
    for(var i = 0; i < 3; i++){
        if(diagPrin[i] == posLastJogada){
            diagPrin.splice(i,1); // remove o elementos a partir da posição colocada splice(pos,quantidade)
            verificar(jogada,diagPrin); // função pra verificar se os outras duas posições do vetor estão marcadas iguais
        }
        if(diagSec[i] == posLastJogada){
            diagSec.splice(i,1); 
            verificar(jogada,diagSec)
        }
        if(linha1[i] == posLastJogada){
            linha1.splice(i,1); 
            verificar(jogada,linha1)
        }
        else if(linha2[i] == posLastJogada){
            linha2.splice(i,1); 
            verificar(jogada,linha2)
        }
        else if(linha3[i] == posLastJogada){
            linha3.splice(i,1); 
            verificar(jogada,linha3)
        }
        if(coluna1[i] == posLastJogada){
            coluna1.splice(i,1);
            verificar(jogada,coluna1)
        }
        else if(coluna2[i] == posLastJogada){
            coluna2.splice(i,1);
            verificar(jogada,coluna2)
        }
        else if(coluna3[i] == posLastJogada){
            coluna3.splice(i,1); 
            verificar(jogada,coluna3)
        }
    }
}

/* Nessa função verifica se os outros dois elementos do vetor também estão marcados iguais
Se estiverem, simboliza que o jogo foi finalizado, e com isso acabouPartida <- 1 
e é printado na tela que acabou a partida e o vencedor*/
function verificar(lastJogada, vector){
    if(document.getElementsByClassName("casa")[vector[0]].innerHTML == lastJogada
        && document.getElementsByClassName("casa")[vector[1]].innerHTML == lastJogada){
            acabouPartida = 1;
            stringVezDe.innerHTML = "Acabou a partida";
            document.getElementById("indicadorVencedor").innerHTML = "O vencedor é o " + lastJogada; 
    }
}