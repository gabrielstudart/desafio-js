var controlePerg = -1; /*variável de controle de perguntas, que vai ser incrementado
                        para ser cada indice do vetor perguntas, começa no -1,
                        pois vai ser incrementado antes de ser usado, se começasse no 0 e fosse incrementado antes
                        iria pegar a partir da segunda pergunta ( indice = 1) */

var somaItens = 0; // variável que vai guardar o somatório das respostas das pessoas para ter o resultado final

//Nessa variável estão todas as perguntas e respostas que vocês usarão no desafio
var perguntas = [
    {
        texto: 'O céu não é o limite',
        respostas: [
            { valor: 0, texto: 'Ter preguiça' },
            { valor: 1, texto: 'Quando der certo a gente faz' },
            { valor: 3, texto: 'Vamo pra cima familia' },
            { valor: 2, texto: 'Se der for viavel a gente faz' }
        ]
    },
    {
        texto: 'Apaixonados pela missão',
        respostas: [
            { valor: 0, texto: 'Projetos nada mais sao do que numeros' },
            { valor: 1, texto: 'Dinheiro é bom e é isto' },
            { valor: 3, texto: 'Se meu projeto nao ajudar meu cliente entao nao fiz nada' },
            { valor: 2, texto: 'Se eu entender o que esta no codigo entao ta show' }
        ]
    },
    {
        texto: 'Ohana',
        respostas: [
            { valor: 0, texto: 'Ta com problema? Se vira filhao' },
            { valor: 3, texto: 'Voce é meu irmao de batalha, to contigo e nao solto' },
            { valor: 2, texto: 'Voce é da gti, eu tambem entao tudo certo' },
            { valor: 1, texto: 'Fora da Gti nem olho' }
        ]
    },
    {
        texto: 'Integridade',
        respostas: [
            { valor: 0, texto: 'Furar a fila do ru' },
            { valor: 2, texto: 'farol amarelo pra mim é verde' },
            { valor: 1, texto: 'Clonar cartao de credito' },
            { valor: 3, texto: 'Sou integro' }
        ]
    },
    {
        texto: 'Ser capitão da nave',
        respostas: [
            { valor: 3, texto: 'Eu serei o lider que eu quero pra mim' },
            { valor: 0, texto: 'Eu nao sabia, entao nao fiz' },
            { valor: 1, texto: 'Ve la, depois me conta' },
            { valor: 2, texto: 'To so esperando meu estagio aprovar pra sair da GTi' }
        ]
    },
    {
        texto: '{1 + [ 1 + 1^2.(1 - 1^(-1) + ( -1)^2)].1} = ?',
        respostas: [
            { valor: 0, texto: '10' },
            { valor: 3, texto: 'Um milhão' },
            { valor: 2, texto: '2' },
            { valor: 1, texto: '3' }
        ]
    }
]
// Nesa função vai ser mostrado a pergunta e as resposta, e vai ser computado a pontuação de cada resposta da pessoa
function mostrarQuestao() {
    var checkItem = document.getElementsByName('resposta');

    /* Nesse if, ele verifica que se pelo menos uma das opções tiver verdadeiro a variável controlePerg pode ser incrementado
    e com isso ele pode apertar o botão e seguir em frente, caso todas estejam falsas a afirmação vai ser falsa, ele não entra no if
    não vai ser incrementado e com isso não vai para a próxima pergunta. Detalhe, como tem que sair da tela inicial e ir para primeira pergunta
    tem uma condição, que se controlePerg == -1, que significa que ainda não começou ( ou acabou), então se apertar o botão tem que incrementar */
    if(checkItem[0].checked != false || checkItem[1].checked != false || 
        checkItem[2].checked != false || checkItem[3].checked != false || controlePerg == -1){

        /* Aqui é feito o calculo da pontuação da pessoa, tem a condição do -1, porque quando for -1 ainda tava na tela inicial,
        ou seja não tinha perguntas. O calculo é feito do valor de cada pergunta vezes se a pergunta foi marcada e soma, como 
        a pergunta marcada o ".checked" retorna true, que é 1, não altera a o valor, e se não foi marcada, é false, ou seja, 0, o produto vai ser 0
        e não vai influenciar*/
        if(controlePerg != -1){
            somaItens = somaItens + checkItem[0].checked*perguntas[controlePerg]['respostas'][0]['valor'] +
                                        checkItem[1].checked*perguntas[controlePerg]['respostas'][1]['valor'] +
                                            checkItem[2].checked*perguntas[controlePerg]['respostas'][2]['valor'] +
                                                checkItem[3].checked*perguntas[controlePerg]['respostas'][3]['valor'];
            
        }
        document.getElementById('resultado').innerHTML = "";
        document.getElementById('confirmar').innerHTML = 'Próxima';
        controlePerg ++;
    }
    // Nesse if só entra até a ultima pergunta, depois vai para o else que finaliza o quiz
    if(controlePerg <=5 ){
    
        document.getElementById('listaRespostas').style.display = 'inline';
        document.getElementById('titulo').innerHTML = perguntas[controlePerg]['texto'];

        for(var i = 0; i < 4; i++){
            checkItem[i].checked = false; // põe todas as perguntas para false, para não ficar a resposta anterior
            document.getElementsByTagName('span')[i].innerHTML = perguntas[controlePerg]['respostas'][i]['texto'];
            document.getElementsByName('resposta')[i].value = perguntas[controlePerg]['respostas'][i]['valor'] // to colocando, mas não to precisando...
            checkItem[i].value = perguntas[controlePerg]['respostas'][i]['valor'];
        }
    }
    else{
        finalizarQuiz();
    }
}

function finalizarQuiz() {
    document.getElementById('titulo').innerHTML = 'QUIZ DOS VALORES DA GTI';
    document.getElementById('listaRespostas').style.display = 'none';
    document.getElementById('resultado').innerHTML = "Sua pontuação foi: " + parseInt((somaItens/18) * 100) + "%";
    document.getElementById('confirmar').innerHTML = 'Refazer Quiz';
    // para poder recomeçar o quiz
    controlePerg = -1; 
    somaItens = 0;
}